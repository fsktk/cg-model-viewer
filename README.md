### Simple 3D Model Viewer with OpenGL

It is a simple model viewer. You can view ```.obj``` model with it. You should specify which ```.obj``` file and ```.DDS``` file you want to view from terminal as command line arguments. 

```
$ mkdir build
$ cd build
$ cmake ../
$ make
$ cd ../model_viewer/
$ ./model_viewer [path to obj file] [path to DDS file]
```

##### Controls:

**Arrow Keys:** rotate the model in x and y axis

**"W, A, S, D" Keys:** rotate the light in x and y axis

**Z:** zoom in    **X:** zoom out

**“1, 2, 3, 4”:** to adjust the light power (1 is lowest; 4 is highest light power)

**“5, 6, 7”:** to change the light color 

**5:**  red    **6:**  green    **7:** blue

**C:** reset the light color

**X:** reset all settings
